unit HugeControl;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils,Controls,Graphics{Tcanvas}
  //,VisibleRectUnit
  ;

type
THugeControl=class;

THugeControlTFPList= class(TFPList)
  function Get(Index: Integer):THugeControl;
  procedure Put(Index: Integer; Item: THugeControl);
  public
    property Items[Index: Integer]: THugeControl read Get write Put;
end;

THugeControl=class(Tcontrol)
protected
  procedure DoSetBounds(ALeft, ATop, AWidth, AHeight : integer);override;
public
  procedure PaintToParentCustomControl;virtual;
  procedure PaintToUnknownParent(ParentCanvas:Tcanvas);virtual;
  procedure PaintToCanvas(VisiblePart:trect;
                         DestCanvas:Tcanvas;DestCanvasRect:Trect);virtual;abstract;
end;

TScaledHugeControl=class(THugeControl)
protected
  //simple Fields:
  FScale:real;
  FSourceWidth,FSourceHeight:word;
  procedure Internal_SetSourceSizes(NewSrcWidth,NewSrcHeight:word);
  procedure Internal_SetSizeByScale(ScaleForSize:real=0);
public
  procedure SetScale(NewScale:real);
  property Scale:real read FScale write SetScale;
  property SourceWidth:word read FSourceWidth;
  property SourceHeight:word read FSourceHeight;
end;

Procedure GetPaintRectsFromParent(parent:TWinControl;const BoundsRect:Trect;
                                  var CanvasPart,VisiblePart:Trect);

implementation

Procedure GetPaintRectsFromParent(parent:TWinControl;const BoundsRect:Trect;
                                  var CanvasPart,VisiblePart:Trect);
begin
CanvasPart:=Trect.Intersect(parent.ClientRect,BoundsRect);
VisiblePart:=CanvasPart;
VisiblePart.Offset(-BoundsRect.Left,-BoundsRect.Top);
end;

procedure THugeControl.DoSetBounds(ALeft, ATop, AWidth, AHeight : integer);
var P:Pinteger;
begin
  P:=@Left;  P^:= ALeft;
  P:=@Top;   P^:= ATop;
  P:=@Width; P^:= AWidth;
  P:=@Height; P^:= AHeight;
  if Parent <> nil then Parent.InvalidatePreferredSize;
end;

procedure THugeControl.PaintToParentCustomControl;
var canvasPart,VisiblePart:trect;
  ParentCanvas:Tcanvas;
begin
if parent=nil then exit;
assert(parent is TCustomControl);
//if (parent is TCustomControl)then
  ParentCanvas:=TCustomControl(parent).Canvas
{else
  exit}
  ;
GetPaintRectsFromParent(parent,BoundsRect,canvasPart,VisiblePart);
PaintToCanvas(VisiblePart,ParentCanvas,canvasPart);
end;

procedure THugeControl.PaintToUnknownParent(ParentCanvas:Tcanvas);
var canvasPart,VisiblePart:trect;
begin
if parent=nil then exit;
GetPaintRectsFromParent(parent,BoundsRect,canvasPart,VisiblePart);
PaintToCanvas(VisiblePart,ParentCanvas,canvasPart);
end;

//--------------------------------------------------------
procedure TScaledHugeControl.Internal_SetSourceSizes(NewSrcWidth,NewSrcHeight:word);
var Szchngd:boolean;
begin
if FSourceWidth<>NewSrcWidth then begin
  FSourceWidth:=NewSrcWidth;
  Szchngd:=true;
end;
if FSourceHeight<>NewSrcHeight then begin
  FSourceHeight:=NewSrcHeight;
  Szchngd:=true;
end;
if Szchngd then
  Internal_SetSizeByScale;
end;

procedure TScaledHugeControl.Internal_SetSizeByScale(ScaleForSize:real=0);
var NewWidth,NewHeight:TConstraintSize;
begin
if ScaleForSize=0 then begin
  if FScale=0 then exit;
  ScaleForSize:=FScale;
end;
NewWidth:={RescaleAsInflate}round(FSourceWidth*ScaleForSize);
NewHeight:={RescaleAsInflate}round(FSourceHeight*ScaleForSize);
setbounds(left,top,NewWidth,NewHeight);
AdjustSize;
end;

procedure TScaledHugeControl.SetScale(NewScale:real);
begin
  FScale:=NewScale;
  Internal_SetSizeByScale;
end;

//----------------------------------------------------------

function THugeControlTFPList.Get(Index: Integer): THugeControl;
begin result:=THugeControl(inherited get(Index));end;

procedure THugeControlTFPList.Put(Index: Integer; Item: THugeControl);
begin inherited Put(Index,Pointer(Item));end;

end.


unit VirtualScrollControl;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils,Controls,StdCtrls{for TScrollBar},
  Forms{for sbVertical},types{Prect, PPoint},
  Graphics{for Tcanvas};

type
TVirtualScrollControl = class;



TVirtualAlignment = class;
TVirtualPoint  = class Position:tpoint;end;
TExtendedScrollBar = class;
TControlFPList= class(TFPList)
  function Get(Index: Integer):TControl;
  procedure Put(Index: Integer; Item: TControl);
  public
    property Items[Index: Integer]: TControl read Get write Put;
end;

TVirtualPositionChangeEvent = procedure(CurrentVirtualPosition: TPoint) of Object;
TControlObserveChangeEvent = procedure(AControl: Tcontrol;observed:boolean;VisibleRect:Trect) of Object;
TObservationListChangeEvent = procedure(Sender: TVirtualScrollControl;NewObserved,ChangedNotObserved:TControlFPList) of Object;
TPaintEvent = procedure(Sender: TVirtualScrollControl;
  AControl: Tcontrol;ControlPart:Trect;
  DestCanvas:Tcanvas;DestCanvasRect:Trect) of Object;

TVisibleField = class({TWinControl}TCustomControl)
  protected
  {
    procedure MouseMove(Shift: TShiftState; X,Y: Integer); override;
    procedure MouseDown(Button: TMouseButton; Shift:TShiftState; X,Y:Integer); override;
    procedure MouseUp(Button: TMouseButton; Shift:TShiftState; X,Y:Integer); override;
    }
 {   function GetParent:TVirtualScrollControl;
    procedure SetParent(NewParent: TVirtualScrollControl); }

 //   procedure DoOnResize;override;
public
  procedure InsertControl(AControl: TControl; Index: integer);override;
  procedure RemoveControl(AControl: TControl);override;
  procedure paint;override;
  //property Parent: TVirtualScrollControl read GetParent write SetParent;
  property OnMouseDown;
  property OnMouseUp;
  property OnMouseMove;
  property MouseCapture;
end;

type TVirtualScrollControl = class(TWinControl)
  private
    FVirtualPosition,VirtualLimitPoint:Tpoint;
    FsuspendCount:integer;
    //LastVisibleIndex:word;

    //Components
    FHorzScrollBar,FVertScrollBar:TExtendedScrollBar{ TScrollBar};
    FChildAlignment:TVirtualAlignment;
    FVisibleField:TVisibleField;

    //need free:
    FViewedControls:TControlFPList;
    procedure OnChangeVerticalScrollBar(Sender: TObject);
    procedure OnChangeHorizontalScrollBar(Sender: TObject);
    procedure DoSuspend;
    procedure UpdateScrollbars;
    function CorrectPositionToVirtualLimitPoint(Position:tpoint):tpoint;
    procedure Internal_ReplaceViewedControlsList
              (NewViewedControlsList,ExcludedFromViewed:TControlFPList);overload;
    procedure Internal_ReplaceViewedControlsList
              (NewViewedControlsList:TControlFPList);overload;
    procedure Internal_NotifyViewedControlsListChanged
              (ExcludedFromViewed:TControlFPList);
    procedure Internal_NotifyViewedControlsChanged;
    function Internal_ControlInVirtualView(AControl:Tcontrol;AControlPoint:TVirtualPoint):boolean;overload;
    function Internal_ControlInVirtualView(ControlIndex:word):boolean;overload;
    function Internal_ControlInCenter(AControl:Tcontrol):boolean;overload;
    function Internal_ControlInCenter(ControlIndex:word):boolean;overload;
    function Internal_ControlVisibleRect(AControl:Tcontrol):Trect;
    function Internal_ControlVisibleRect(CanvasPartForControl:Trect):Trect;
    function Internal_ControlVisibleRect(AControl:Tcontrol;
                                     var CanvasPartForControl:Trect):Trect;
    function Internal_CanvasPartFor(AControl:Tcontrol):Trect;
  protected
    procedure OnChangeVirtualAlignment(FromIndex:word=0);
    procedure OnRemoveControl(AControl: TControl);//заменить control на индекс, для исключения поиска в списке
    procedure OnInsertControl(AControl: TControl);
    procedure DoOnResize;override;
    function DoMouseWheel(Shift: TShiftState; WheelDelta: Integer; MousePos: TPoint): Boolean; override;
    function DoMouseWheelHorz(Shift: TShiftState; WheelDelta: Integer; MousePos: TPoint): Boolean;override;
    function GetVirtualCenterPoint:tpoint;
    procedure SetVirtualCenterPoint(Newcenter:tpoint);
    procedure AlignAsHiden(AControl: TControl);overload;
    procedure AlignAsHiden(index:longword);overload;
    procedure AlignAsVisible(AControl: TControl;NewLeft,NewTop:integer);overload;
    procedure AlignAsVisible(index:longword);overload;
    procedure CallControlsPaint;
    procedure ManualControlPaint(Cntrl:Tcontrol);
  public
    ControlAtClientRectCenter:Tcontrol;
    MouseMoveCursor,MouseDefaultCursor:TCursor;
    OnChangeVirtualPosition:TVirtualPositionChangeEvent;
    OnChangeControlObserve: TControlObserveChangeEvent;
    OnChangeViewedControls: TNotifyEvent;
    OnChangeControlsObsevation:TObservationListChangeEvent;
    OnPaintControl:TPaintEvent;
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure Suspend;
    procedure Resume;
    procedure UpdateVirtualLimitPoint;
    function GetViewedControlsList:TControlFPList;
    function Suspended: Boolean;
//    procedure CaptureMouseDown(Button: TMouseButton; Shift: TShiftState;X, Y: Integer);
    procedure Scrollby(dx,dy:integer);override;
    procedure ScrollInView(AControl: TControl);
    procedure ScrollInViewByIndex(ControlIndex:cardinal);
    procedure ScrollToPoint(ToPoint:tpoint;FromScrollbars:boolean=false);
    property VirtualAlignment:TVirtualAlignment read FChildAlignment;
    property VirtualPosition:tpoint read FVirtualPosition;
    property VirtualCenterPoint:tpoint read GetVirtualCenterPoint write SetVirtualCenterPoint;
    procedure DoAlignOnVisibleArea(FromIndex:cardinal=0);
    procedure ReAlignOnVisibleArea(FromIndex:cardinal=0);    //not implemenet
    property HorzScrollBar:TExtendedScrollBar{ TScrollBar} read FHorzScrollBar{ write SetHorzScrollBar};
    property VertScrollBar:TExtendedScrollBar{ TScrollBar} read FVertScrollBar{ write SetVertScrollBar};
    property VisibleField:TVisibleField read FVisibleField;
    property OnMouseWheel;
    property OnMouseMove;
    property MouseCapture;
    property OnMouseDown;
end;

function IsLinesOverlayed(Line1x1,line1x2,line2x1,line2x2:integer):boolean;inline;
function GetListOfExcluded(OrigList,ChangedList:TFPlist):TControlFPList;

type TVirtualAlignment = class
private
    Control: TVirtualScrollControl;
    FControlsPerLine: integer;
    VirtualPositionList,VirtualControllist:TFPList{array of TVirtualPoint};
    FElementsHeight,FElementsWidth:cardinal;
    FLayout: TControlChildrenLayout;
    FHorizontalSpacing,FVerticalSpacing: integer;
    FOnChange: TNotifyEvent;
    Fsuspended:boolean;
    {FTopBottomSpacing: integer;
    FVerticalSpacing: integer;}
    procedure DoVirtualAlign(FromIndex:word=0);
    procedure DoSuspend(SuspendIt:Boolean);
    function GetControl(const Index: Integer): TControl;
    function GetControlCount: Integer;
    procedure OnResizeControl(Sender: TObject);
    procedure SetControlsPerLine(const AValue: integer);
    procedure SetLayout(const AValue: TControlChildrenLayout);
    procedure SetHorizontalSpacing(const AValue: integer);
    procedure SetVerticalSpacing(const AValue: integer);
    procedure getElementsWidth;
    procedure getElementsHeight;
  public
    property ControlCount: Integer read GetControlCount;
    property Controls[Index: Integer]: TControl read GetControl;
    function AddControl(AControl: TControl): cardinal;
    procedure InsertControl(AControl: TControl; Index: cardinal);
    constructor Create(OwnerControl: TVirtualScrollControl);
    procedure Change(FromIndex:word=0);
    function RemoveControl(Item:{ Pointer}TControl): Integer;
    function GetControlIndex(AControl: TControl): integer;
    function GetControlVirtualPoint(AControl: TControl): TVirtualPoint;
    function GetVirtualPosition(Index: cardinal): TVirtualPoint;
    procedure SetVirtualPosition(Index: word;VirtualX,VirtualY:longint);
    procedure VirtualReAlign(FromIndex:word=0);
    property Suspended: Boolean read Fsuspended write DoSuspend;
    property ControlsPerLine: integer read FControlsPerLine write SetControlsPerLine default 1;
    property ElementsWidth:cardinal read FElementsWidth;
    property ElementsHeight:cardinal read FElementsHeight;

//    property VirtualPosition[Index: word]: TVirtualPoint read GetVirtualPosition write SetVirtualPosition; default;
//    property ScrollBarsParent:TWinControl read GetScrollBarsParent write SetScrollBarsParent;
    //procedure ScrollInView(AControl: TControl);
//    procedure UpdateScrollbars;
    property HorizontalSpacing: integer read FHorizontalSpacing write SetHorizontalSpacing;
    property OnChange: TNotifyEvent read FOnChange write FOnChange;
    property VerticalSpacing: integer read FVerticalSpacing write SetVerticalSpacing;
    {property HorizontalSpacing: integer read FHorizontalSpacing write SetHorizontalSpacing default 0;}
    //property VerticalSpacing: integer read FVerticalSpacing write SetVerticalSpacing default 0;
    {property EnlargeHorizontal: TChildControlResizeStyle read FEnlargeHorizontal
                           write SetEnlargeHorizontal default crsAnchorAligning;
    property EnlargeVertical: TChildControlResizeStyle read FEnlargeVertical
                             write SetEnlargeVertical default crsAnchorAligning;
    property ShrinkHorizontal: TChildControlResizeStyle read FShrinkHorizontal
                            write SetShrinkHorizontal default crsAnchorAligning;
    property ShrinkVertical: TChildControlResizeStyle read FShrinkVertical
                              write SetShrinkVertical default crsAnchorAligning;}


    property Layout: TControlChildrenLayout read FLayout write SetLayout;
  end;

type TExtendedScrollBar = class(TScrollBar)
  private
    OnChangeEvent: TNotifyEvent;//will apply then mousedown & disapply when mouseup
  protected
    procedure MouseEnter;override;
    procedure MouseLeave;override;
    procedure MouseMove(Shift: TShiftState; X,Y: Integer);override;
    procedure MouseDown(Button: TMouseButton; Shift:TShiftState; X,Y:Integer); override;
    procedure MouseUp(Button: TMouseButton; Shift:TShiftState; X,Y:Integer); override;
end;

implementation

function MaxOfTwo(First,Second:integer):integer;
begin if First>Second then result:=First else result:=Second;end;

{function MinOfTwo(First,Second:integer):integer;
begin if First<Second then result:=First else result:=Second;end;}

function IsPointInLine(p,linex1,linex2:integer):boolean;inline;
begin
result:=false;
if (p=linex1)or(p=linex2)then
  result:=true
else
if linex1<linex2 then begin
  if(p>linex1)and(p<linex2) then begin result:=true;exit;end;
end else begin
  if (p<linex1)and(p>linex2) then begin result:=true;exit;end;
end;
end;

function IsLinesOverlayed(Line1x1,line1x2,line2x1,line2x2:integer):boolean;inline;
begin
result:=false;
if IsPointInLine(Line1x1,line2x1,line2x2)then begin result:=true;exit;end;
if IsPointInLine(Line1x2,line2x1,line2x2)then begin result:=true;exit;end ;
if IsPointInLine(Line2x1,line1x1,line1x2)then begin result:=true;exit;end  ;
if IsPointInLine(Line2x2,line1x1,line1x2)then begin result:=true;exit;end   ;
end;

function IsPointInRect(p:tpoint;Field:Trect):boolean;inline;
begin
if (Field.Left<p.x)and(p.x<Field.Right)and(Field.Top<p.y)and(p.y<Field.Bottom)then
  exit
else
  result:=false;
end;

{function DiffTFPlistElements(First,Second:TFPlist):boolean;
var Fenum,Senum:TFPListEnumerator;
ElementExist:boolean;
begin
if (First.Count<>Second.Count) then exit(true);
if (First.Count=0)then exit(false);
Fenum:=First.GetEnumerator;result:=false;
while ((not result) and Fenum.MoveNext)do begin
   ElementExist:=false;Senum:=Second.GetEnumerator;
   while ((not ElementExist) and Senum.MoveNext)do begin
     ElementExist:=Fenum.Current=Senum.Current;
     //writeln(booltostr(ElementExist,'element Equals','element Diff'));
   end;
   //writeln();
   if (not ElementExist)then result:=true;
   //freeAndNil(Senum);
   Senum.Free;
end;
Fenum.Free;//freeAndNil(Fenum);
end;}

function GetListOfExcluded(OrigList,ChangedList:TFPlist):TControlFPList;
begin
if (OrigList.Count=ChangedList.Count)and
   (OrigList.Count=0)
then
  exit(nil);

result:=TControlFPList.Create;
result.Assign(OrigList,laSrcUnique,ChangedList);
end;

//--------------------------------------------
function TControlFPList.Get(Index: Integer): TControl;
begin result:=TControl(inherited get(Index));end;

procedure TControlFPList.Put(Index: Integer; Item: TControl);
begin inherited Put(Index,Pointer(Item));end;


//-----------TVisibleField-----------
procedure TVisibleField.InsertControl(AControl: TControl; Index: integer);
begin
inherited;
TVirtualScrollControl(Owner).OnInsertControl(AControl);
end;

procedure TVisibleField.RemoveControl(AControl: TControl);
begin
TVirtualScrollControl(Owner).OnRemoveControl(AControl);
inherited;
end;
{
function TVisibleField.GetParent:TVirtualScrollControl;
begin
  result:=TVirtualScrollControl(inherited Parent);
end;

procedure TVisibleField.SetParent(NewParent: TVirtualScrollControl);
begin
inherited SetParent(NewParent);
end;
}
{
procedure TVisibleField.MouseMove(Shift: TShiftState; X,Y: Integer);
begin
inherited;
Parent.MouseMove(Shift,X,Y);
end; }

{procedure TVisibleField.MouseDown(Button: TMouseButton; Shift:TShiftState; X,Y:Integer);
begin
inherited;
Parent.MouseDown(Button,Shift,X,Y);
end; }

{procedure TVisibleField.MouseUp(Button: TMouseButton; Shift:TShiftState; X,Y:Integer);
begin
inherited;
{Parent.MouseUp(Button,Shift,X,Y);}
end; }

procedure TVisibleField.Paint;
begin
if controlcount>0 then TVirtualScrollControl(parent).CallControlsPaint;
inherited;
end;

{procedure TVisibleField.DoOnResize;
begin
{no mater if it earlier than TVirtualScrollControl }
inherited;
end;}

//-----------TVirtualScrollControl-----------
constructor TVirtualScrollControl.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  BorderStyle:=bsSingle;
  FVirtualPosition.Create(-1,-1);{//why?}

  MouseMoveCursor:=crSizeAll;MouseDefaultCursor:=crDefault;{//temp}

  FVisibleField:=TVisibleField.Create(self);VisibleField.Parent:=self;
  VisibleField.Align:=alClient;
  FVertScrollBar:=TExtendedScrollBar.Create(self);
  FHorzScrollBar:=TExtendedScrollBar.Create(self);
  FVertScrollBar.Kind:=sbVertical;FHorzScrollBar.Kind:=sbHorizontal;
  FVertScrollBar.Align:=alRight;FHorzScrollBar.Align:=alBottom;
  FVertScrollBar.Parent:=self;FHorzScrollBar.Parent:=self;
  FVertScrollBar.ShowHint:=true;FHorzScrollBar.ShowHint:=true;

  FVertScrollBar.SmallChange:=10;FHorzScrollBar.SmallChange:=10;
  FVertScrollBar.OnChangeEvent:=@OnChangeVerticalScrollBar;
  FHorzScrollBar.OnChangeEvent:=@OnChangeHorizontalScrollBar;

 FViewedControls:=TControlFPList.Create;



  FChildAlignment:=TVirtualAlignment.Create(Self);
end;

destructor TVirtualScrollControl.Destroy;
begin
//Suspended:=true;

inherited;
FViewedControls.Free;
end;

procedure TVirtualScrollControl.Suspend;
begin
  self.DoSuspend;
end;

procedure TVirtualScrollControl.Resume;
begin
  FsuspendCount:=FsuspendCount-1;
if (FsuspendCount=0)and(VirtualAlignment<>nil) then
  VirtualAlignment.Suspended:=false;
end;

function TVirtualScrollControl.Suspended: Boolean;
begin
result:=FsuspendCount>0;
end;

function TVirtualScrollControl.GetViewedControlsList:TControlFPList;
begin
result:=TControlFPList.Create;
if FViewedControls.Count>0 then
  result.Assign(FViewedControls);
end;

procedure TVirtualScrollControl.OnChangeVerticalScrollBar(Sender: TObject);
begin
//SetFocus;{//зачем? не решено, см. TExtendedScrollBar.MouseLeave}
ScrollToPoint(point(FVirtualPosition.X,VertScrollBar.Position),true);
//debugln('OnChangeVerticalScrollBar',booltostr( (VertScrollBar.OnChangeEvent<>nil),true));
end;

procedure TVirtualScrollControl.OnChangeHorizontalScrollBar(Sender: TObject);
begin
SetFocus;
ScrollToPoint(point(HorzScrollBar.Position,FVirtualPosition.Y),true);
end;

procedure TVirtualScrollControl.OnRemoveControl(AControl: TControl);
begin
FViewedControls.Remove(AControl);{//need? it first, because:}
FChildAlignment.RemoveControl(AControl);//it will call realign and deleted control go to excluded list
end;

procedure TVirtualScrollControl.UpdateScrollbars;
var max:integer;
begin
if assigned(FVertScrollbar)and assigned(FHorzScrollbar)  then begin
  max:=MaxOfTwo(VirtualLimitPoint.y,0);
  FVertScrollbar.SetParams(FVirtualPosition.y,0,max);
  if max=0 then FVertScrollbar.Hide else FVertScrollbar.Show;
  FVertScrollbar.Hint:=inttostr(max);

  max:=MaxOfTwo(VirtualLimitPoint.x,0);
  HorzScrollBar.SetParams(FVirtualPosition.x,0,Max);
  if max=0 then HorzScrollBar.Hide else HorzScrollBar.Show;
  HorzScrollBar.Hint:=inttostr(max);
end;
end;

procedure TVirtualScrollControl.UpdateVirtualLimitPoint;
var max{,max2}:integer;
begin
max:=VirtualAlignment.ElementsHeight-VisibleField.ClientHeight;
//max2:=VirtualAlignment.ElementsHeight-(ClientHeight shr 1);
if max<0 then VirtualLimitPoint.y:=round(max/2) else VirtualLimitPoint.y:=max;

max:=VirtualAlignment.ElementsWidth-VisibleField.ClientWidth;
//max2:=VirtualAlignment.ElementsWidth-(ClientWidth shr 1);
if max<0 then VirtualLimitPoint.x:=round(max/2) else VirtualLimitPoint.x:=max;

end;

function TVirtualScrollControl.Internal_ControlInVirtualView(AControl:Tcontrol;AControlPoint:TVirtualPoint):boolean;
begin
result:=(IsLinesOverlayed(AControlPoint.Position.x,AControlPoint.Position.x+AControl.Width,
                    FVirtualPosition.x,FVirtualPosition.x+ClientWidth))
  and(IsLinesOverlayed(AControlPoint.Position.y,AControlPoint.Position.y+AControl.Height,
                    FVirtualPosition.y,FVirtualPosition.y+ClientHeight));
end;

function TVirtualScrollControl.Internal_ControlInVirtualView(ControlIndex:word):boolean;
begin
result:=(
Internal_ControlInVirtualView(
          tcontrol(FChildAlignment.VirtualControllist.Items[ControlIndex]),
          TVirtualPoint(FChildAlignment.VirtualPositionList.Items[ControlIndex])
                      )
        );
end;

function TVirtualScrollControl.Internal_ControlInCenter(AControl:Tcontrol):boolean;
begin
result:=AControl.BoundsRect.Contains(point(Width shr 1,Height shr 1));
end;

function TVirtualScrollControl.Internal_ControlInCenter(ControlIndex:word):boolean;
begin
result:=Internal_ControlInCenter
          (Tcontrol
            (FChildAlignment.VirtualControllist.Items[ControlIndex])
           );
end;

function TVirtualScrollControl.Internal_ControlVisibleRect(AControl:Tcontrol):Trect;
begin
{result:=Rect(-MinOfTwo(0,AControl.Left),-MinOfTwo(0,AControl.Top),
    MinOfTwo(VisibleField.width-AControl.Left,AControl.Width),
    MinOfTwo(VisibleField.Height-AControl.Top,AControl.Height));}
result:=Trect.Intersect(AControl.BoundsRect,self.VisibleField.ClientRect);
result.Offset(-AControl.Left,-AControl.Top);
end;

function TVirtualScrollControl.Internal_ControlVisibleRect
                       (AControl:Tcontrol;var CanvasPartForControl:Trect):Trect;
begin
CanvasPartForControl:=Trect.Intersect(AControl.BoundsRect,
                                      VisibleField.ClientRect);
result:=CanvasPartForControl;
result.Offset(-AControl.Left,-AControl.Top);
end;

function TVirtualScrollControl.Internal_ControlVisibleRect
                               (CanvasPartForControl:Trect):Trect;
begin
result:=CanvasPartForControl;
result.Offset(-CanvasPartForControl.Left,-CanvasPartForControl.Top);
end;

function TVirtualScrollControl.Internal_CanvasPartFor(AControl:Tcontrol):Trect;
begin
result:=Trect.Intersect(AControl.BoundsRect,VisibleField.ClientRect);
end;

procedure TVirtualScrollControl.ScrollInView(AControl: TControl);
var n:integer;
begin
//n:=VisibleField.GetControlIndex(AControl);
n:=VirtualAlignment.VirtualControllist.IndexOf(AControl);
if n>-1 then ScrollInViewByIndex(n);
end;

procedure TVirtualScrollControl.ScrollInViewByIndex(ControlIndex:cardinal);
begin
//if VisibleField.ControlCount>ControlIndex then
if VirtualAlignment.VirtualControllist.Count>ControlIndex then
  ScrollToPoint(FChildAlignment.GetVirtualPosition(ControlIndex).Position);
end;

procedure TVirtualScrollControl.ScrollToPoint(ToPoint:tpoint;FromScrollbars:boolean=false);
begin
ToPoint:=CorrectPositionToVirtualLimitPoint(ToPoint);
if not(FVirtualPosition=ToPoint) then begin
  FVirtualPosition:=ToPoint;
  DoAlignOnVisibleArea;
  if FromScrollbars=false then UpdateScrollbars;
  if assigned(OnChangeVirtualPosition)then OnChangeVirtualPosition(FVirtualPosition);
end;

end;

procedure TVirtualScrollControl.Scrollby(dx,dy:integer{;FromScrollbars:boolean=false});
begin
ScrollToPoint(point(FVirtualPosition.x+dx,FVirtualPosition.y+dy){,FromScrollbars});
end;

function TVirtualScrollControl.CorrectPositionToVirtualLimitPoint(Position:tpoint):tpoint;
begin
if VirtualLimitPoint.x>0 then begin
  if Position.x>VirtualLimitPoint.x then Position.x:=VirtualLimitPoint.x;
  if Position.x<0 then Position.x:=0;
end else Position.x:=VirtualLimitPoint.x;
if VirtualLimitPoint.y>0 then begin
if Position.y>VirtualLimitPoint.y then Position.y:=VirtualLimitPoint.y;
if Position.y<0 then Position.y:=0;
end else Position.y:=VirtualLimitPoint.y;
result:=Position;
end;

procedure TVirtualScrollControl.DoAlignOnVisibleArea(FromIndex:cardinal=0);
var n:word;
//control:tcontrol;controlPoint:TVirtualPoint;
NewViewedControls{,ExcludedFromViewed}:TControlFPList;
begin
//ReAlignOnVisibleArea(FromIndex);exit;
ControlAtClientRectCenter:=nil;
if (FChildAlignment.VirtualControllist.Count<1)
or (FChildAlignment.VirtualControllist.Count<=FromIndex)
or (FsuspendCount>0)
then exit;

if FChildAlignment.VirtualControllist.Count=1 then begin
  ControlAtClientRectCenter:=tcontrol(FChildAlignment.VirtualControllist.Items[0]);
  AlignAsVisible(0);
  if (FViewedControls.Count=1) then begin
    if(FViewedControls.Items[0]<>tcontrol(FChildAlignment.VirtualControllist.Items[0])) then begin
      FViewedControls.Items[0]:=tcontrol(FChildAlignment.VirtualControllist.Items[0]);
      Internal_NotifyViewedControlsChanged();
    end;
  end else begin
    if FViewedControls.Count>1 then
      FViewedControls.Clear;
    FViewedControls.Add(tcontrol(FChildAlignment.VirtualControllist.Items[0]));
    Internal_NotifyViewedControlsChanged;
  end;
exit;
end;

NewViewedControls:=TControlFPList.Create;

for n:=FromIndex to FChildAlignment.VirtualControllist.Count-1 do begin
  //find observing state after alinging on Visible Area
  if Internal_ControlInVirtualView(n) then begin
    if (ControlAtClientRectCenter=nil)and Internal_ControlInCenter(n)then begin
      ControlAtClientRectCenter:=Tcontrol(FChildAlignment.VirtualControllist.Items[n]);
    end;
    NewViewedControls.Add(FChildAlignment.VirtualControllist.Items[n]);
    AlignAsVisible(n);
  end else begin
    AlignAsHiden(n);
  end;
end;
Internal_ReplaceViewedControlsList(NewViewedControls);
end;//DoAlignOnVisibleArea

procedure TVirtualScrollControl.Internal_ReplaceViewedControlsList
          (NewViewedControlsList,ExcludedFromViewed:TControlFPList);
var Listchanged:boolean;
begin
Listchanged:=( (ExcludedFromViewed<>nil)and(ExcludedFromViewed.Count>1) )
         or(FViewedControls.Count<>NewViewedControlsList.Count);

if Listchanged then begin
  FViewedControls.Free;
  FViewedControls:=NewViewedControlsList;
  Internal_NotifyViewedControlsListChanged(ExcludedFromViewed);
end;
end;

procedure TVirtualScrollControl.Internal_ReplaceViewedControlsList
          (NewViewedControlsList:TControlFPList);
begin
  FViewedControls.Free;
  FViewedControls:=NewViewedControlsList;
  Internal_NotifyViewedControlsChanged;
end;

procedure TVirtualScrollControl.Internal_NotifyViewedControlsChanged;
begin
if assigned(OnChangeViewedControls)then
     OnChangeViewedControls(self);
end;

procedure TVirtualScrollControl.Internal_NotifyViewedControlsListChanged
          (ExcludedFromViewed:TControlFPList);
begin
if assigned(OnChangeControlsObsevation)then
     OnChangeControlsObsevation(self,FViewedControls,ExcludedFromViewed);
end;

procedure TVirtualScrollControl.ReAlignOnVisibleArea(FromIndex:cardinal=0);
var n:integer;
NewLastVisibleIndex:word;
control:tcontrol;controlPoint:TVirtualPoint;
VisibleFounded,Control_N_Visible:boolean;
NewViewedControls,ExcludedFromViewed:TControlFPList;

procedure CheckControlInView;
begin
if Internal_ControlInVirtualView(control,controlPoint) then begin
  Control_N_Visible:=true;
  VisibleFounded:=true;
  NewLastVisibleIndex:=n;{//need use as index of ControlAtClientRectCenter}

  NewViewedControls.Add(control);
  if (ControlAtClientRectCenter=nil)and
     (control.BoundsRect.Contains(point(Width shr 1,Height shr 1)))
  then begin
    ControlAtClientRectCenter:=control;
  end;
  AlignAsVisible(control,controlPoint.Position.x-FVirtualPosition.x,
    controlPoint.Position.y-FVirtualPosition.y);
end else begin
  Control_N_Visible:=false;
  //AlignAsHiden(control);
end;
//writeln(n,booltostr(Control_N_Visible,'Visible','Invisible'));
end;

procedure CheckVisibilityToFront;
begin
repeat
  controlPoint:=TVirtualPoint(FChildAlignment.VirtualPositionList.Items[n]);
  control:=tcontrol(FChildAlignment.VirtualControllist.Items[n]);
  CheckControlInView;
  n:=n+1;
until (  (VisibleFounded and(Control_N_Visible=false)) or
         (n>=FChildAlignment.VirtualControllist.Count)
       );
end;

procedure CheckVisibilityBack;
begin
repeat
  controlPoint:=TVirtualPoint(FChildAlignment.VirtualPositionList.Items[n]);
  control:=tcontrol(FChildAlignment.VirtualControllist.Items[n]);
  CheckControlInView;
  n:=n-1;
until (  (VisibleFounded and(Control_N_Visible=false)) or (n<0)  );
end;

begin

{ not implemented
if (FsuspendCount>0)
or (FChildAlignment.VirtualControllist.Count<=FromIndex) then exit;

if (FChildAlignment.VirtualControllist.Count<1)then begin
  ControlAtClientRectCenter:=nil;
{  if (FViewedControls.Count>0)then begin

    FViewedControls.Clear;
    Internal_NotifyViewedControlsListChanged;
  end;}//no need, see TVirtualScrollControl.OnRemoveControl
  exit;
end;

if FChildAlignment.VirtualControllist.Count=1 then begin
  control:=tcontrol(FChildAlignment.VirtualControllist.Items[0]);
  controlPoint:=TVirtualPoint(FChildAlignment.VirtualPositionList.Items[0]);
  ControlAtClientRectCenter:=control;
  AlignAsVisible(control,controlPoint.Position.x-FVirtualPosition.x,
    controlPoint.Position.y-FVirtualPosition.y);
  {if (FViewedControls.Count=1) then begin
    if(FViewedControls.Items[0]<>control) then begin
      FViewedControls.Items[0]:=control;
      Internal_NotifyViewedControlsListChanged();
    end;
  end else begin
    if FViewedControls.Count>1 then
      FViewedControls.Clear;
    FViewedControls.Add(control);
    Internal_NotifyViewedControlsListChanged;
  end;}//no need, see TVirtualScrollControl.OnRemoveControl
exit;
end;

ControlAtClientRectCenter:=nil;
NewViewedControls:=TControlFPList.Create;
//if last visible control was last in visible field and was deleted:
if LastVisibleIndex>FChildAlignment.VirtualControllist.Count-1 then begin
   LastVisibleIndex:=FChildAlignment.VirtualControllist.Count-1;
end;
//loocking from last viewed to last index
n:=LastVisibleIndex; VisibleFounded:=false;
{writeln();
writeln('LastVisibleIndex:',n);
writeln('CheckVisibilityToFront:');}
CheckVisibilityToFront;
//loocking from last viewed downto first index, try start prev index
if LastVisibleIndex>0 then begin
  n:=LastVisibleIndex-1;
  //writeln('CheckVisibilityBack:');
  CheckVisibilityBack;
end;//LastVisibleIndex>0

LastVisibleIndex:=NewLastVisibleIndex;
ExcludedFromViewed:=GetListOfExcluded(FViewedControls,NewViewedControls);
if (ExcludedFromViewed<>nil)and(ExcludedFromViewed.Count>0)then
  for n:=0 to ExcludedFromViewed.Count-1 do begin
    AlignAsHiden(Tcontrol(ExcludedFromViewed.Items[n]));
//    writeln('Excluded index: ',FChildAlignment.VirtualControllist.IndexOf(ExcludedFromViewed.Items[n]));
  end;
//writeln('ExcludedFromViewed: ',ExcludedFromViewed.Count);
Internal_ReplaceViewedControlsList(NewViewedControls,ExcludedFromViewed);

ExcludedFromViewed.Free;
}
end;

procedure TVirtualScrollControl.AlignAsHiden(AControl: TControl);
begin
//unable to set bounds out of range 32000 for big controls, USE visiblity
if AControl.Visible then begin
  AControl.Visible:=false;
  if assigned(OnChangeControlObserve) then
    OnChangeControlObserve(Acontrol,false,Rect(0,0,0,0));
end;
end;

procedure TVirtualScrollControl.AlignAsHiden(index:longword);
begin
AlignAsHiden(tcontrol(FChildAlignment.VirtualControllist.Items[index]));
end;

procedure TVirtualScrollControl.AlignAsVisible(AControl: TControl;NewLeft,NewTop:integer);
var WasVisible,Moved:boolean;
begin
WasVisible:=AControl.Visible;
Moved:=(AControl.Left<>NewLeft)or(AControl.Top<>NewTop);
if Moved then
  AControl.SetBounds(NewLeft,NewTop,AControl.Width,AControl.Height);
if not WasVisible then
  AControl.Visible:=true;
if {(Moved or (not WasVisible) )and}(assigned(OnChangeControlObserve)) then
    OnChangeControlObserve(Acontrol,true,
    {Rect(-MinOfTwo(0,NewLeft),-MinOfTwo(0,NewTop),
         MinOfTwo(VisibleField.width-NewLeft,AControl.Width),
         MinOfTwo(VisibleField.Height-NewTop,AControl.Height))}
    Internal_ControlVisibleRect(AControl));

end;

procedure TVirtualScrollControl.AlignAsVisible(index:longword);overload;
var control:tcontrol;controlPoint:TVirtualPoint;
begin
controlPoint:=TVirtualPoint(FChildAlignment.VirtualPositionList.Items[index]);
control:=tcontrol(FChildAlignment.VirtualControllist.Items[index]);
AlignAsVisible(control,controlPoint.Position.x-FVirtualPosition.x,
    controlPoint.Position.y-FVirtualPosition.y);
end;

procedure TVirtualScrollControl.CallControlsPaint;
var n:dword;
Notpainted:Tcontrol;
//CanvClipRect:Trect;
{var ControlVisiblePart,CanvasPartForControl:Trect;}
function ControlNeedManualPaint(Cntrl:Tcontrol):boolean;
begin
result:=not((Cntrl is TGraphicControl)or(Cntrl is TCustomControl));
end;

begin
if Suspended then exit;
if OnPaintControl=nil then exit;
if FViewedControls.Count>0 then begin
  //CanvClipRect:=VisibleField.Canvas.ClipRect;
//  VisifieldCli:=VisibleField.ClientRect;
  for n:=0 to FViewedControls.Count-1 do begin
     Notpainted:=FViewedControls.Items[n];
     if ControlNeedManualPaint(Notpainted)then begin
       ManualControlPaint(Notpainted);
       {ControlVisiblePart:=Internal_ControlVisibleRect
                           (Notpainted,CanvasPartForControl);
       if CanvClipRect.IntersectsWith(CanvasPartForControl)then begin
         OnPaintControl(self,Notpainted,ControlVisiblePart,
                        VisibleField.Canvas,CanvasPartForControl);
       end;}
     end;
  end;
end;
end;

procedure TVirtualScrollControl.ManualControlPaint(Cntrl:Tcontrol);
var {cntrlbounds,VisifieldCli,}DestCanvasRect,CanvasPartForControl,ControlVisiblePart:Trect;
begin
{cntrlbounds:=Cntrl.BoundsRect;
VisifieldCli:=VisibleField.ClientRect;
DestCanvasRect:=Trect.Intersect(Cntrl.BoundsRect,VisibleField.ClientRect);
OnPaintControl(self,Cntrl,Internal_ControlVisibleRect(Cntrl),
              VisibleField.Canvas,DestCanvasRect);
 }
ControlVisiblePart:=Internal_ControlVisibleRect(Cntrl,CanvasPartForControl);
if VisibleField.Canvas.ClipRect.IntersectsWith(CanvasPartForControl)then begin
  OnPaintControl(self,Cntrl,ControlVisiblePart,
                        VisibleField.Canvas,CanvasPartForControl);
end;
VisibleField.Canvas.Brush.Style:=bsClear;
VisibleField.Canvas.Brush.Color:=clred;
VisibleField.Canvas.Pen.Color:=clred;
VisibleField.Canvas.Rectangle(DestCanvasRect);
end;

procedure TVirtualScrollControl.DoOnResize;
begin
if (FsuspendCount=0)
then begin
  UpdateVirtualLimitPoint;
  FVirtualPosition:=CorrectPositionToVirtualLimitPoint(FVirtualPosition);
  DoAlignOnVisibleArea;
  UpdateScrollbars;
end;
inherited;
end;

{procedure TVirtualScrollControl.InsertControl(AControl: TControl; Index: integer);
begin
//if assigned(VirtualAlignment)then raise EInvalidOperation.Create('Use VisibleFiled as parent')
{else} inherited;
end;}

procedure TVirtualScrollControl.OnInsertControl(AControl: TControl);
begin
AControl.Hide;//Default state,see DoAlignOnVisibleArea
VirtualAlignment.AddControl(AControl);
end;

function TVirtualScrollControl.DoMouseWheel(Shift: TShiftState; WheelDelta: Integer;
  MousePos: TPoint): Boolean;
begin
result:=inherited;
if result then exit;
Scrollby(0,-WheelDelta);
end;

function TVirtualScrollControl.DoMouseWheelHorz(Shift: TShiftState; WheelDelta: Integer;
  MousePos: TPoint): Boolean;
begin
result:=inherited;
if result then exit;
Scrollby(-WheelDelta,0);
end;

procedure TVirtualScrollControl.OnChangeVirtualAlignment(FromIndex:word=0){(Sender: TObject)};
begin
if (FsuspendCount>0)
then
  exit;
UpdateVirtualLimitPoint;
FVirtualPosition:=CorrectPositionToVirtualLimitPoint(FVirtualPosition);
DoAlignOnVisibleArea(FromIndex);
UpdateScrollbars;
end;

procedure TVirtualScrollControl.DoSuspend;
begin
FsuspendCount:=FsuspendCount+1;
if (FsuspendCount=1)and(VirtualAlignment<>nil) then
  VirtualAlignment.Suspended:=true;
end;

{procedure TVirtualScrollControl.CaptureMouseDown(Button: TMouseButton; Shift: TShiftState;X, Y: Integer);
begin
MouseDown(Button,Shift,X, Y);MouseCapture:=true;
end;}

function TVirtualScrollControl.GetVirtualCenterPoint:tpoint;
begin
result.Create(FVirtualPosition.x+(ClientWidth shr 1),FVirtualPosition.y+(ClientHeight shr 1));
end;

procedure TVirtualScrollControl.SetVirtualCenterPoint(Newcenter:tpoint);
begin
ScrollToPoint(point(Newcenter.x-(ClientWidth shr 1),Newcenter.y-(ClientHeight shr 1)));
end;




//---TVirtualAlignment---//
constructor TVirtualAlignment.Create(OwnerControl: TVirtualScrollControl);
begin
  inherited Create;
  Control := OwnerControl;
  VirtualControllist:=TFPList.Create;
  VirtualPositionList:=TFPList.Create;
  FControlsPerLine:=1;
end;

procedure TVirtualAlignment.SetControlsPerLine(const AValue: integer);
begin
  if (FControlsPerLine=AValue) or (AValue<1) then exit;
  FControlsPerLine:=AValue;Change;
end;

procedure TVirtualAlignment.Change(FromIndex:word=0);
begin
if self.Suspended then exit;
  DoVirtualAlign(FromIndex);
  getElementsHeight;getElementsWidth;
  if Assigned(FOnChange) then
    FOnChange(Self);
  Control.OnChangeVirtualAlignment;
end;

procedure TVirtualAlignment.SetLayout(const AValue: TControlChildrenLayout);
begin if FLayout=AValue then exit;FLayout:=AValue;Change; end;

procedure TVirtualAlignment.VirtualReAlign(FromIndex:word=0);
var WasSuspended:boolean;
begin
WasSuspended:=Fsuspended;
Fsuspended:=false;
DoVirtualAlign(FromIndex);
getElementsHeight;getElementsWidth;{// так же и в change}
Fsuspended:=WasSuspended;
end;

function TVirtualAlignment.GetVirtualPosition(Index: cardinal): TVirtualPoint;
begin
If(Index >=VirtualPositionList.Count) then result:=nil;
Result:=TVirtualPoint(VirtualPositionList.Items[Index]);
end;

procedure TVirtualAlignment.DoVirtualAlign(FromIndex:word=0);
var PositionInLine:word;
controlVirtualPoint,PrevVirtualPoint:TVirtualPoint;
CurrentControl,PrevControl:tcontrol;
maxlengthInPrevline,PrevElementEndPosition:cardinal;

function GetmaxlengthInPrevline(CurrentIndex:word):cardinal;
var PrevLineFirstElement,PrevLineEndElement,n:word;
begin
result:=0;
if CurrentIndex>=ControlsPerLine then begin
  PrevLineEndElement:=ControlsPerLine*(CurrentIndex div ControlsPerLine)-1;
  PrevLineFirstElement:=PrevLineEndElement-ControlsPerLine+1;
  if (Layout=cclTopToBottomThenLeftToRight) then
    for n:=PrevLineFirstElement to PrevLineEndElement do begin
       controlVirtualPoint:=TVirtualPoint(VirtualPositionList.Items[n]);
       CurrentControl:=tcontrol(VirtualControllist.Items[n]);
       if result<CurrentControl.Width+controlVirtualPoint.Position.x then
          result:=CurrentControl.Width+controlVirtualPoint.Position.x;
    end;

  if (Layout=cclLeftToRightThenTopToBottom) then
    for n:=PrevLineFirstElement to PrevLineEndElement do begin
       controlVirtualPoint:=TVirtualPoint(VirtualPositionList.Items[n]);
       CurrentControl:=tcontrol(VirtualControllist.Items[n]);
       if result<CurrentControl.Height+controlVirtualPoint.Position.y then
          result:=CurrentControl.Height+controlVirtualPoint.Position.y;
    end;


end;

end;

begin//DoVirtualAlign
if (FromIndex>=VirtualControllist.Count)or(FLayout=cclNone)or Fsuspended then exit;
if FromIndex=0 then begin
  controlVirtualPoint:=TVirtualPoint(VirtualPositionList.Items[FromIndex]);
  CurrentControl:=tcontrol(VirtualControllist.Items[FromIndex]);
  controlVirtualPoint.Position.x:={FHorizontalSpacing+}0;
  controlVirtualPoint.Position.y:={FVerticalSpacing+}0;
  FromIndex:=FromIndex+1;
  if FromIndex>=VirtualControllist.Count then exit;
  if (FLayout=cclTopToBottomThenLeftToRight)then
     maxlengthInPrevline:=CurrentControl.Width+controlVirtualPoint.Position.x;
  if (FLayout=cclLeftToRightThenTopToBottom)then
     maxlengthInPrevline:=CurrentControl.Height+controlVirtualPoint.Position.y;
end else begin
maxlengthInPrevline:=GetmaxlengthInPrevline(FromIndex);
end;
PositionInLine:=FromIndex-ControlsPerLine*(FromIndex div ControlsPerLine);
while (FromIndex<VirtualControllist.Count)do begin
  controlVirtualPoint:=TVirtualPoint(VirtualPositionList.Items[FromIndex]);
  CurrentControl:=tcontrol(VirtualControllist.Items[FromIndex]);
  if (Layout=cclTopToBottomThenLeftToRight)then begin
    if PositionInLine=0 then begin
      controlVirtualPoint.Position.y:=0;//initial top or VirtualPositionList.Items[0]
      controlVirtualPoint.Position.x:=FHorizontalSpacing+maxlengthInPrevline;
      //PrevElementEndPosition:=0;
      maxlengthInPrevline:=CurrentControl.Width+controlVirtualPoint.Position.x;
    end else begin
      PrevControl:=tcontrol(VirtualControllist.Items[FromIndex-1]);
      PrevVirtualPoint:=TVirtualPoint(VirtualPositionList.Items[FromIndex-1]);
      PrevElementEndPosition:=PrevVirtualPoint.Position.y+PrevControl.Height;
      controlVirtualPoint.Position.y:=FVerticalSpacing+PrevElementEndPosition;
      controlVirtualPoint.Position.x:=PrevVirtualPoint.Position.x;
      if maxlengthInPrevline<CurrentControl.Width+controlVirtualPoint.Position.x then
         maxlengthInPrevline:=CurrentControl.Width+controlVirtualPoint.Position.x;
    end;
  end;

  if (Layout=cclLeftToRightThenTopToBottom)then begin
      if PositionInLine=0 then begin
        controlVirtualPoint.Position.x:=0;//initial left or VirtualPositionList.Items[0]
        controlVirtualPoint.Position.y:=FVerticalSpacing+maxlengthInPrevline;
        maxlengthInPrevline:=CurrentControl.Height+controlVirtualPoint.Position.y;
      end else begin
        PrevControl:=tcontrol(VirtualControllist.Items[FromIndex-1]);
        PrevVirtualPoint:=TVirtualPoint(VirtualPositionList.Items[FromIndex-1]);
        PrevElementEndPosition:=PrevVirtualPoint.Position.x+PrevControl.Width;
        controlVirtualPoint.Position.x:=FHorizontalSpacing+PrevElementEndPosition;
        controlVirtualPoint.Position.y:=PrevVirtualPoint.Position.y;
        if maxlengthInPrevline<CurrentControl.Height+controlVirtualPoint.Position.y then
         maxlengthInPrevline:=CurrentControl.Height+controlVirtualPoint.Position.y;
      end;
    end;

  inc(FromIndex);inc(PositionInLine);if (PositionInLine mod ControlsPerLine =0) then PositionInLine:=0;
end;//while


end;//DoVirtualAlign

procedure TVirtualAlignment.SetVirtualPosition(Index: word;VirtualX,VirtualY:longint);
begin
if Layout=cclNone then
  TVirtualPoint(VirtualPositionList.Items[Index]).Position.Create(VirtualX,VirtualY);
end;

function TVirtualAlignment.AddControl(AControl: TControl): cardinal;
begin
result:=VirtualControllist.Count;
InsertControl(AControl,result);
end;

procedure TVirtualAlignment.InsertControl(AControl: TControl; Index: cardinal);
begin
//if out of range
if VirtualControllist.IndexOf(AControl)>=0 then exit;//move?
if Index>VirtualControllist.Count then Index:=VirtualControllist.Count;
VirtualControllist.Insert(Index,AControl);
VirtualPositionList.Insert(Index,TVirtualPoint.Create);
AControl.AddHandlerOnResize(@OnResizeControl);
Change(Index);
end;

function TVirtualAlignment.RemoveControl(Item:{ Pointer}TControl): Integer;
begin
Result := VirtualControllist.IndexOf(Item);
If Result > -1 then begin
  VirtualControllist.Delete(Result);
  VirtualPositionList.Delete(Result);
  Item.RemoveHandlerOnResize(@OnResizeControl);
  change(Result);
end;
end;

procedure TVirtualAlignment.getElementsWidth;
var n:integer; mostvalue:cardinal;
begin
FElementsWidth:=0;
if VirtualControllist.Count<1 then exit;
  for n:=0 to VirtualControllist.Count-1 do begin
      mostvalue:=TVirtualPoint(VirtualPositionList.Items[n]).Position.x+
                      tcontrol(VirtualControllist.Items[n]).Width;
      if mostvalue>FElementsWidth then FElementsWidth:=mostvalue;
  end;
end;

procedure TVirtualAlignment.getElementsHeight;
var n:integer; mostvalue:cardinal;
begin
FElementsHeight:=0;
if VirtualControllist.Count<1 then exit;
  for n:=0 to VirtualControllist.Count-1 do begin
      mostvalue:=TVirtualPoint(VirtualPositionList.Items[n]).Position.y+
                      tcontrol(VirtualControllist.Items[n]).Height;
      if mostvalue>FElementsHeight then FElementsHeight:=mostvalue;
  end;
end;

function TVirtualAlignment.GetControlVirtualPoint(AControl: TControl): TVirtualPoint;
var n:integer;
begin
n:=VirtualControllist.IndexOf(AControl);
if n<0 then begin
  result:=TVirtualPoint.Create;
end else result:=TVirtualPoint(VirtualPositionList.Items[n]);
end;

procedure TVirtualAlignment.SetVerticalSpacing(const AValue: integer);
begin
  if FVerticalSpacing=AValue then exit;
  FVerticalSpacing:=AValue;
  Change;
end;

procedure TVirtualAlignment.SetHorizontalSpacing(const AValue: integer);
begin
  if FHorizontalSpacing=AValue then exit;
  FHorizontalSpacing:=AValue;
  Change;
end;

function TVirtualAlignment.GetControl(const Index: Integer): TControl;
begin
if (Index<0)or(Index>=VirtualControllist.Count) then Result :=nil
else Result := TControl(VirtualControllist.Items[Index]);
end;

function TVirtualAlignment.GetControlCount: Integer;
begin
  if VirtualControllist <> nil then
    Result := VirtualControllist.Count
  else
    Result := 0;
end;

function TVirtualAlignment.GetControlIndex(AControl: TControl): integer;
begin
  if VirtualControllist <> nil then
    Result := VirtualControllist.IndexOf(AControl)
  else
    Result := -1;
end;

procedure TVirtualAlignment.OnResizeControl(Sender: TObject);
var n:integer;
begin
if self.Suspended then exit;
n:=VirtualControllist.IndexOf(Tcontrol(Sender));
change(n);
end;

procedure TVirtualAlignment.DoSuspend(SuspendIt:Boolean);
begin
Fsuspended:=SuspendiT;
if Fsuspended=false then Change;
end;



//--------------------------TExtendedScrollBar

procedure TExtendedScrollBar.MouseDown(Button: TMouseButton; Shift: TShiftState;X, Y: Integer);
begin
{OnChange:=OnChangeEvent;
debugln('MouseDown');
MouseCapture:=true;}
inherited;

end;

procedure TExtendedScrollBar.MouseUp(Button: TMouseButton; Shift: TShiftState;X, Y: Integer);
begin
inherited;
{OnChange:=nil;}
end;

procedure TExtendedScrollBar.MouseMove(Shift: TShiftState; X, Y: Integer);begin
inherited;
//debugln('OnChange=nil:'+booltostr(OnChange=nil,true));
end;

procedure TExtendedScrollBar.MouseEnter;
begin
inherited;
OnChange:=OnChangeEvent;
end;

procedure TExtendedScrollBar.MouseLeave;
begin
inherited;
OnChange:=nil;
end;


end.

